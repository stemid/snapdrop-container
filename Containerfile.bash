#!/usr/bin/env bash

set -xeu

author='Stefan Midjich <swehack at gmail dot com>'
server_ctr=$(buildah from docker.io/node:lts-alpine)
curl -Ls https://api.github.com/repos/RobinLinus/snapdrop/tarball/master | tar -xzf - -C /tmp

buildah config --user node \
  --workingdir /home/node \
  --entrypoint '["/usr/local/bin/node", "index.js"]' \
  --port 3000 \
  "$server_ctr"

buildah copy --chown node "$server_ctr" /tmp/RobinLinus-snapdrop-*/server/
buildah run "$server_ctr" -- npm i
buildah commit "$server_ctr" localhost/snapdrop:server

client_ctr=$(buildah from registry.gitlab.com/stemid/tinyhttpd-container:darkhttpd)
buildah copy --chown 0:0 "$client_ctr" /tmp/RobinLinus-snapdrop-*/client/
buildah commit "$client_ctr" localhost/snapdrop:client
